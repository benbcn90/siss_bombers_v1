package com.bombers.x7044508e.siss_bombers.bean;

public class Desastre{
    private int numero;
    private Double latitud;
    private Double longitud;
    private int nivelPrioridad;
    private String descripcion;

    public Desastre(){

    }

    public Desastre(int numero, Double latitud, Double longitud, int nivelPrioridad) {
        this.numero = numero;
        this.latitud = latitud;
        this.longitud = longitud;
        this.nivelPrioridad = nivelPrioridad;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public int getNivelPrioridad() {
        return nivelPrioridad;
    }

    public void setNivelPrioridad(int nivelPrioridad) {
        this.nivelPrioridad = nivelPrioridad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Desastre{" +
                "numero='" + numero + '\'' +
                ", latitud='" + latitud + '\'' +
                ", longitud='" + longitud + '\'' +
                ", nivelPrioridad='" + nivelPrioridad + '\'' +
                '}';
    }

}
