package com.bombers.x7044508e.siss_bombers.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.bombers.x7044508e.siss_bombers.R;
import com.bombers.x7044508e.siss_bombers.bean.DesastreExcel;

import java.util.ArrayList;
import java.util.Arrays;

public class DataListExcelAdapter extends ArrayAdapter<String> {

    public static ArrayList<DesastreExcel> desastreExcelList;
//    private ArrayList<String> mSpinnerItems;
    private Activity context;

    public DataListExcelAdapter(Activity context, ArrayList<DesastreExcel> desastreExcelList) {
        super(context, R.layout.layout_row_excel_view, desastreExcelList.size());
//        this.mSpinnerItems = spinnerItems;
        this.context = context;
        this.desastreExcelList = desastreExcelList;
    }

    @Override
    public int getCount() {
        return desastreExcelList.size();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_row_excel_view, null, true);

        TextView textViewNumero = (TextView) listViewItem.findViewById(R.id.textViewNumero);
        TextView textViewGrupo = (TextView) listViewItem.findViewById(R.id.textViewGrupo);
        TextView textViewTipo = (TextView) listViewItem.findViewById(R.id.textViewTipus);
        TextView textViewMunicipio = (TextView) listViewItem.findViewById(R.id.textViewMunicipi);
        Spinner spinner = (Spinner) listViewItem.findViewById(R.id.spinnerNivelPrioridad);
//        ImageView iv = (ImageView)listViewItem.findViewById(R.id.imageView3);


        textViewNumero.setText("Nombre: " + desastreExcelList.get(position).getNumero());
        textViewGrupo.setText("Grup: " + desastreExcelList.get(position).getGrup());
        textViewTipo.setText("Tipus: " + desastreExcelList.get(position).getTipus());
        textViewMunicipio.setText("Municipi: " + desastreExcelList.get(position).getMunicipi());

        ArrayList<String> numbers = new ArrayList<String>(Arrays.asList(context.getResources().getStringArray(R.array.numbers)));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, numbers);
        spinner.setAdapter(adapter);
        spinner.setSelection(Integer.parseInt(desastreExcelList.get(position).getNivelPrioridad())-1);
        // Uri uri = Uri.parse(uImages[position]);
        //Uri uri = Uri.parse("https://drive.google.com/uc?id=0B___GhMLUVtOY09SbDU5cDU2T1U");
        // draweeView.setImageURI(uri);

//        Picasso.with(context).load(uImages[position]).into(iv);

        return listViewItem;
    }
}