package com.bombers.x7044508e.siss_bombers.util;
import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bombers.x7044508e.siss_bombers.bean.DesastreExcel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class UtilitiesAssignmentXLSFile extends BaseAdapter {
    public static ArrayList<DesastreExcel> desastreExcelInicialList;
    public static ArrayList<DesastreExcel> desastreExcelFiltradoList;
    public static ArrayList<String> tipologiaFiltroList = new ArrayList<>();
    public static File directoryExcel;
    public static InputStream inputStream;
    private Context context;

    public UtilitiesAssignmentXLSFile(Context context){
        this.context = context;
    }

    public void convertURIToInputStream(Uri uri) throws FileNotFoundException {
        inputStream = context.getContentResolver().openInputStream(uri);
    }

    public static void extraerTipologiasFiltros(ArrayList<DesastreExcel> desastreExcelList){
        for(DesastreExcel desastreExcel: desastreExcelList){
            tipologiaFiltroList.add(desastreExcel.getGrup()+"_"+desastreExcel.getTipus().substring(0,2));
        }
        Set<String> set = new HashSet<>(tipologiaFiltroList);
        tipologiaFiltroList.clear();
        tipologiaFiltroList.addAll(set);
    }

    //Si hay tipologias filtradas se hace el filtrado, sino se añade todos los valores del excel sin filtro
    public static void filtrarListaExcel(ArrayList<String> filtrosSeleccionados){
        if(!filtrosSeleccionados.isEmpty()){
            desastreExcelFiltradoList = new ArrayList<>();

            for(DesastreExcel desastreExcel: desastreExcelInicialList){
                for(String tipologia : filtrosSeleccionados){
                    if(tipologia.equals(desastreExcel.getGrup()+"_"+desastreExcel.getTipus().substring(0,2))){
                        desastreExcelFiltradoList.add(desastreExcel);
                    }
                }
            }
        }else{
            desastreExcelFiltradoList = new ArrayList<>();
            desastreExcelFiltradoList = new ArrayList<>(desastreExcelInicialList);
        }
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }

}
