package com.bombers.x7044508e.siss_bombers.bean;

import java.util.Date;

public class Asignacion {
    private FurgonSalvamento furgonSalvamento;
    private Desastre desastre;
    private int nivelPrioridad;
    private Double distanciaEntrePuntos;
    private Date fechaAsignacion;

    public Asignacion() {

    }

    public Asignacion(FurgonSalvamento furgonSalvamento, Desastre desastre, int nivelPrioridad, Double distanciaEntrePuntos, Date fechaAsignacion) {
        this.furgonSalvamento = furgonSalvamento;
        this.desastre = desastre;
        this.nivelPrioridad = nivelPrioridad;
        this.distanciaEntrePuntos = distanciaEntrePuntos;
        this.fechaAsignacion = fechaAsignacion;
    }

    public FurgonSalvamento getFurgonSalvamento() {
        return furgonSalvamento;
    }

    public void setFurgonSalvamento(FurgonSalvamento furgonSalvamento) {
        this.furgonSalvamento = furgonSalvamento;
    }

    public Desastre getDesastre() {
        return desastre;
    }

    public void setDesastre(Desastre desastre) {
        this.desastre = desastre;
    }

    public int getNivelPrioridad() {
        return nivelPrioridad;
    }

    public void setNivelPrioridad(int nivelPrioridad) {
        this.nivelPrioridad = nivelPrioridad;
    }

    public Double getDistanciaEntrePuntos() {
        return distanciaEntrePuntos;
    }

    public void setDistanciaEntrePuntos(Double distanciaEntrePuntos) {
        this.distanciaEntrePuntos = distanciaEntrePuntos;
    }

    public Date getFechaAsignacion() {
        return fechaAsignacion;
    }

    public void setFechaAsignacion(Date fechaAsignacion) {
        this.fechaAsignacion = fechaAsignacion;
    }

    @Override
    public String toString() {
        return "Asignacion{" +
                "nombreFurgonSalvamento='" + furgonSalvamento.getNombre() + '\'' +
                ", nombreLugarDesastre='" + desastre.getNumero() + '\'' +
                ", nivelPrioridad='" + nivelPrioridad + '\'' +
                ", distanciaEntrePuntos=" + distanciaEntrePuntos +
                ", fechaAsignacion=" + fechaAsignacion +
                '}';
    }
}
