package com.bombers.x7044508e.siss_bombers.util;

public class Configuration {

    /*En este segmento se crean variables estaticas con el fin de armar la url para solicitar que Google SpreadSheets
    devuelva un JSON y con la informacion solicitada.
     */
//    public static final String APP_SCRIPT_WEB_APP_URL = "https://script.google.com/macros/s/AKfycbzTQfznVatC3rAP0n4uzp_tPSqLCbn5EgbLGNpG9MkLt0ZAG2I/exec";
    public static final String APP_SCRIPT_WEB_APP_URL = "https://spreadsheets.google.com/feeds/list/1pcywA2ZBgqaBDeZRxxMwL1WNlRJSQNowEnBgbUGvVrU/default/public/values?alt=json";
    public static final String ADD_USER_URL = APP_SCRIPT_WEB_APP_URL;
//    public static final String LIST_USER_URL = APP_SCRIPT_WEB_APP_URL+"?action=readAll";
    public static final String LIST_DATA_URL = APP_SCRIPT_WEB_APP_URL;

    public static final String KEY_NOMBRE = "gsx$nombre";
    public static final String KEY_LATITUD = "gsx$latitud";
    public static final String KEY_LONGITUD = "gsx$longitud";
    public static final String KEY_ES_BOMBERO = "gsx$esbombero";
    public static final String KEY_NIVEL_PRIORIDAD = "gsx$nivelprioridad";
    public  static final String KEY_ACTION = "action";

    public static final String KEY_LIST = "entry";




}
