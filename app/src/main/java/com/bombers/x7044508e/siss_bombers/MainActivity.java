package com.bombers.x7044508e.siss_bombers;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bombers.x7044508e.siss_bombers.adapter.DataListExcelAdapter;
import com.bombers.x7044508e.siss_bombers.controller.DataListExcel;
import com.bombers.x7044508e.siss_bombers.util.UtilitiesAssignmentKlmFile;
import com.bombers.x7044508e.siss_bombers.util.UtilitiesAssignmentXLSFile;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private Button openKlmFile, openXlsFile, asignarBomberosCatastrofes;
    public final String[] EXTERNAL_PERMS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
    };
    public final int EXTERNAL_REQUEST = 138;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openKlmFile=(Button)findViewById(R.id.buttonOpenKlmFile);
        openXlsFile=(Button)findViewById(R.id.buttonOpenXlsFile);
        asignarBomberosCatastrofes = (Button) findViewById(R.id.buttonAsignar);

        requestForPermission();

        //Al iniciar la aplicacion deshabilitamos el boton de asignar hasta que se tenga ficheros validos par arealizar la asignacion
        asignarBomberosCatastrofes.setEnabled(false);

        openXlsFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                startActivityForResult(intent, 001);
            }
        });

        openKlmFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent().setType("application/vnd.google-earth.kml+xml").setAction(Intent.ACTION_GET_CONTENT);
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                startActivityForResult(intent, 002);
            }
        });

        asignarBomberosCatastrofes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(intent);

                //Iniciamos la lecutra cada 5 segundos después de la primera asignacion
//                startTimer();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Obtenemos la ruta en String del directorio seleccionado
        String selectedPath = data.getData().getPath();
        int startPosition = selectedPath.indexOf(":")+1;
        String fileSubstringPath = selectedPath.substring(startPosition, selectedPath.length());
        String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + fileSubstringPath;

        //Validamos el directorio y trabajamos con su contenido segun sea un Excel o un KML
        if(requestCode == 001 && resultCode == RESULT_OK) {
            try {
                //Validamos y guardamos lso datos del fichero para su postetrior uso
                if(validDirectory(new File(filePath), requestCode, data)){
                    Intent intent = new Intent(getApplicationContext(), DataListExcel.class);
                    startActivity(intent);
                }else{
                    alertDialog();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if(requestCode == 002 && resultCode == RESULT_OK){
            try {
                //Validamos y guardamos lso datos del fichero para su postetrior uso
                if(validDirectory(new File(filePath), requestCode, data)){
                    asignarBomberosCatastrofes.setEnabled(true);
                }else{
                    alertDialog();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    //Comprueba si el directorio seleccionado contiene un fichero valido
    //si contiene un fochero con el que se pueda trabajar, guardamos los datos en sus respectivas clases Utilities
    private boolean validDirectory(File fileDirectory, int requestCode, Intent data) throws FileNotFoundException {
        Uri selectedFile;
        if(requestCode == 001){
            Collection<File> files = FileUtils.listFiles(fileDirectory, new String[]{"xlsx"}, true);
            if(!files.isEmpty()){
                File file = files.iterator().next();
                UtilitiesAssignmentXLSFile.inputStream = new FileInputStream(file);
                UtilitiesAssignmentXLSFile.directoryExcel = fileDirectory;
                return true;
            }
        }else if(requestCode == 002){
            Collection<File> files = FileUtils.listFiles(fileDirectory, new String[]{"kml"}, true);
            if(!files.isEmpty()){
                File file = files.iterator().next();
                UtilitiesAssignmentKlmFile.inputStream = new FileInputStream(file);
                UtilitiesAssignmentKlmFile.directoryKML = fileDirectory;
                return true;
            }
        }
        return false;
    }



    public boolean requestForPermission() {

        boolean isPermissionOn = true;
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            if (!canAccessExternalSd()) {
                isPermissionOn = false;
                requestPermissions(EXTERNAL_PERMS, EXTERNAL_REQUEST);
            }
        }

        return isPermissionOn;
    }

    public boolean canAccessExternalSd() {
        return (hasPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this, perm));

    }

    private void alertDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("No se ha encontrado ningún fichero para leer, pruebe seleccionando otro directorio.");
        dialog.setTitle("Directorio no válido");
        dialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(),"Directorio seleccionado no válido.", Toast.LENGTH_LONG).show();
            }
        });
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }


}
