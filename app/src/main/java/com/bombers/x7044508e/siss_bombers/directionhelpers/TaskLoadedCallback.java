package com.bombers.x7044508e.siss_bombers.directionhelpers;

/**
 * Created by Vishal on 10/20/2018.
 */

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
