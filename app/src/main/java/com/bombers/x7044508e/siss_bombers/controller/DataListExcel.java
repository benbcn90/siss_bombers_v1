package com.bombers.x7044508e.siss_bombers.controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.bombers.x7044508e.siss_bombers.MainActivity;
import com.bombers.x7044508e.siss_bombers.R;
import com.bombers.x7044508e.siss_bombers.adapter.DataListExcelAdapter;
import com.bombers.x7044508e.siss_bombers.bean.DesastreExcel;
import com.bombers.x7044508e.siss_bombers.util.UtilitiesAssignmentXLSFile;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class DataListExcel extends AppCompatActivity {
    private ListView listView;
    Button buttonExcelAceptar;

    Button buttonFiltrar;
    AlertDialog myDialog;

    //Ventana de filtrado
    boolean[] filtrosSeleccionados;
    ArrayList selectedItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_list_excel);
        listView = (ListView) findViewById(R.id.listExcelView);

        buttonExcelAceptar=(Button)findViewById(R.id.buttonExcelAceptar);
        buttonFiltrar = findViewById(R.id.buttonExcelFiltrar);

        buttonExcelAceptar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //Si no se ha realizado ningun filtrado, se selecciona todas las catastrofes del excel por defecto
//                if(selectedItems == null){
                UtilitiesAssignmentXLSFile.filtrarListaExcel(UtilitiesAssignmentXLSFile.tipologiaFiltroList);
//                }

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });


        //Aceptamos la seleccion del filtrado que hayamos echo
        buttonFiltrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlert();
            }
        });

        try {
            //LEE LICHEROS XLSX
            readXlsxFileFromAssets();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Si se ha extraido datos del Excel mostramos el listView
        if(!UtilitiesAssignmentXLSFile.desastreExcelInicialList.isEmpty()){
            DataListExcelAdapter dataListExcelAdapter = new DataListExcelAdapter(this, UtilitiesAssignmentXLSFile.desastreExcelInicialList);
            listView.setAdapter(dataListExcelAdapter);
        }
    }

    public void readXlsxFileFromAssets() throws IOException {
        // we create an XSSF Workbook object for our XLSX Excel File
        XSSFWorkbook workbook = new XSSFWorkbook(UtilitiesAssignmentXLSFile.inputStream);

        // we get first sheet
        XSSFSheet sheet = workbook.getSheetAt(0);

        // we iterate on rows
        Iterator<Row> rowIt = sheet.iterator();

        UtilitiesAssignmentXLSFile.desastreExcelInicialList = new ArrayList<>();

        int catastrofeContar = 0;
        while(rowIt.hasNext()) {
            Row row = rowIt.next();

            // iterate on cells for the current row
            Iterator<Cell> cellIterator = row.cellIterator();
            DesastreExcel desastreExcel = new DesastreExcel();

            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                System.out.print(cell.toString() + ";");

                if(cell.getRowIndex() > 4 && row.getRowNum() <= sheet.getPhysicalNumberOfRows()){
                    if(cell.getColumnIndex() == 0){
                        desastreExcel.setNumero(Integer.parseInt(cell.toString()));
                    }else if(cell.getColumnIndex() == 1){
                        desastreExcel.setUtm_x(cell.toString());
                    }else if(cell.getColumnIndex() == 2) {
                        desastreExcel.setUtm_y(cell.toString());
                    }else if(cell.getColumnIndex() == 3){
                        desastreExcel.setGrup(cell.toString());
                    }else if(cell.getColumnIndex() == 4) {
                        desastreExcel.setPer(cell.toString());
                    }else if(cell.getColumnIndex() == 5) {
                        desastreExcel.setVeh(cell.toString());
                    }else if(cell.getColumnIndex() == 6) {
                        desastreExcel.setTipus(cell.toString());
                    }else if(cell.getColumnIndex() == 7) {
                        desastreExcel.setMunicipi(cell.toString());
                    }else if(cell.getColumnIndex() == 8) {
                        desastreExcel.setUrgent(cell.toString());
                    }else if(cell.getColumnIndex() == 9) {
                        desastreExcel.setnDocs(cell.toString());
                    }else if(cell.getColumnIndex() == 10) {
                        desastreExcel.setTrge(cell.toString());
                    }else if(cell.getColumnIndex() == 11){
                        desastreExcel.setIssi_trge(cell.toString());
                    }
                    //Le damos a todas las catastrofces un nivel deprioridad por defecto de valor 1
                    desastreExcel.setNivelPrioridad("3");
                }
            }
            //Guardo las catastrofces obtenidas del excel en un Array
            if(desastreExcel.getNumero() != 0){
                UtilitiesAssignmentXLSFile.desastreExcelInicialList.add(desastreExcel);
            }

            if(row.getRowNum() > 4){
                //Solo aumenta de latercera fila, donde empeizan los datos de las catastrofes
                catastrofeContar++;
            }
            System.out.println();
        }

        workbook.close();
    }

    private void showAlert(){
        AlertDialog.Builder myBuilder = new AlertDialog.Builder(this);
        //DATA SOURCE
        UtilitiesAssignmentXLSFile.extraerTipologiasFiltros(UtilitiesAssignmentXLSFile.desastreExcelInicialList);
        final CharSequence[] filtroTipologiaList =  UtilitiesAssignmentXLSFile.tipologiaFiltroList.toArray(new CharSequence[UtilitiesAssignmentXLSFile.tipologiaFiltroList.size()]);
        selectedItems = new ArrayList();
        filtrosSeleccionados = filtrosSeleccionados != null ? filtrosSeleccionados : new boolean[UtilitiesAssignmentXLSFile.tipologiaFiltroList.size()];

        //SET PROPERTIES USING METHOD CHAINING
        myBuilder.setTitle("Science Tips").setMultiChoiceItems(filtroTipologiaList, filtrosSeleccionados, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {

                if(isChecked){
                    //Si el usuario selecciona el item, se agrega en la  lista de itemes seleccionados
                    selectedItems.add(filtroTipologiaList[position]);
                    filtrosSeleccionados[position] = true;
                }else if(selectedItems.contains(position)){
                    //Si el item ya estáen la lista dearray se elimina del Array
                    selectedItems.remove(Integer.valueOf(position));
                    filtrosSeleccionados[position] = false;
                }
            }
        });
        myBuilder.setPositiveButton("Filtrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                UtilitiesAssignmentXLSFile.filtrarListaExcel(selectedItems);
                DataListExcelAdapter dataListExcelAdapter = new DataListExcelAdapter(DataListExcel.this, UtilitiesAssignmentXLSFile.desastreExcelFiltradoList);
                listView.setAdapter(dataListExcelAdapter);
                Toast.makeText(DataListExcel.this, "Se ha filtrado correctamente", Toast.LENGTH_SHORT).show();

            }
        });
        //CREATE DIALOG
        myDialog = myBuilder.create();
        //SHOW DIALOG
        myDialog.show();
    }
}
