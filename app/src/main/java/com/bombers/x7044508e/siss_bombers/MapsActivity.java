package com.bombers.x7044508e.siss_bombers;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;

import com.bombers.x7044508e.siss_bombers.controller.DataListExcel;
import com.bombers.x7044508e.siss_bombers.directionhelpers.FetchURL;
import com.bombers.x7044508e.siss_bombers.directionhelpers.TaskLoadedCallback;
import com.bombers.x7044508e.siss_bombers.bean.Asignacion;
import com.bombers.x7044508e.siss_bombers.bean.DesastreExcel;
import com.bombers.x7044508e.siss_bombers.geocoordinateconverter.GeoCoordinateConverter;
import com.bombers.x7044508e.siss_bombers.util.UtilitiesAssignmentKlmFile;
import com.bombers.x7044508e.siss_bombers.util.UtilitiesAssignmentXLSFile;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.data.kml.KmlLayer;

import org.apache.commons.io.FileUtils;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, TaskLoadedCallback {

    private static GoogleMap mMap;
    private Polyline currentPolyline;
    private ArrayList<Marker> mMarkerArray = new ArrayList<Marker>();
    private static ArrayList<Asignacion> asignacionList;
    private KmlLayer kmlLayer;

    //Timer code
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        cargarValoresDelMapa();

        //Iniciamos la lecutra cada 5 segundos después de la primera asignacion
        startTimer();
    }

    private void retrieveFileFromResource() {
        try {
            kmlLayer = new KmlLayer(mMap, UtilitiesAssignmentKlmFile.inputStream, getApplicationContext
                    ());
            kmlLayer.addLayerToMap();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    private void showKMLFileIntoGoogleMaps() throws ExecutionException, InterruptedException {
        UtilitiesAssignmentKlmFile kmlFileParser = new UtilitiesAssignmentKlmFile(getApplicationContext());
        kmlFileParser.adaptKmlFileToGoogleMaps(kmlLayer);
    }

    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + getString(R.string.google_maps_key);
        return url;
    }

    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null) {
//            currentPolyline.remove();
        }
        Random rnd = new Random();
        int randomColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        PolylineOptions polylineOptions = (PolylineOptions) values[0];
        polylineOptions.width(12);
        polylineOptions.color(randomColor);
        currentPolyline = mMap.addPolyline(polylineOptions);
    }


    @Override
    public void onResume() {
        super.onResume();

        if(mMap != null){
            mMap.clear();
            cargarValoresDelMapa();
        }
    }

    private void cargarValoresDelMapa(){
        //AQUI OBTENEMOS LOS BOMBEROS DEL KML
        retrieveFileFromResource();
        //Le damos valor a los marker options para que se muestren en el mapa
        try {
            showKMLFileIntoGoogleMaps();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        //Dibuja las rutas entre un furgon de bomberos y un desastre asignado
        asignacionList = UtilitiesAssignmentKlmFile.getAssignmentsList();


        //TODO:HAY QUE CONTROLAR SI HAY CATATROFES PARA ASIGNAR O NO
        //ITERAMOS POR CATASTROFES
        for (DesastreExcel desastreExcel : UtilitiesAssignmentXLSFile.desastreExcelFiltradoList) {
            Asignacion asignacion = new Asignacion();
            for(Asignacion asig : asignacionList){
                if(asig.getDesastre().getNumero() == desastreExcel.getNumero()){
                    asignacion = asig;
                }
            }

            StringBuilder ubicacionUTM = new StringBuilder("31T").append(" ").append(desastreExcel.getUtm_x()).append(" ").append(desastreExcel.getUtm_y());
            double[] coordenada = GeoCoordinateConverter.getInstance().utm2LatLon(ubicacionUTM.toString());

            //UBICACIONES DE CATASTROFES
            StringBuilder titulo = new StringBuilder("").
                    append(" Nombre: ").append(desastreExcel.getNumero()).append('\n').
                    append(" Grup: ").append(desastreExcel.getGrup()).append('\n').
                    append(" Tipus: ").append(desastreExcel.getTipus()).append('\n').
                    append(" Municipi: ").append(desastreExcel.getMunicipi()).append('\n').
                    append(" Assignat a: ").append(asignacion.getFurgonSalvamento().getNombre());
            mMap.addMarker(new MarkerOptions().position(new LatLng(coordenada[0], coordenada[1])).title(titulo.toString()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marcador_flood_02)));
        }

        //AQUI OBTENEMOS LOS BOMBEROS DEL KML (2)
        if (UtilitiesAssignmentKlmFile.markerOptionsList != null && !UtilitiesAssignmentKlmFile.markerOptionsList.isEmpty()) {
            for (MarkerOptions markerOptions : UtilitiesAssignmentKlmFile.markerOptionsList) {
//                String nombreFurgon = markerOptions.getTitle();
                int startPosition = markerOptions.getTitle().indexOf(" ")+1;
                int endPosition = markerOptions.getTitle().indexOf('\n');
                String nombreFurgon = markerOptions.getTitle().substring(startPosition, endPosition);

                for (Asignacion asing : asignacionList) {
                    if (asing.getFurgonSalvamento().getNombre().equals(nombreFurgon)) {
                        markerOptions.title(markerOptions.getTitle()+'\n'+" Assignat a: "+asing.getDesastre().getNumero());
                    }
                }

                mMap.addMarker(markerOptions);
            }
        }

        for (Asignacion asignacion : asignacionList) {
            LatLng furgonPosicion = new LatLng(asignacion.getFurgonSalvamento().getLatitud(), asignacion.getFurgonSalvamento().getLongitud());
            LatLng desastrePosicion = new LatLng(asignacion.getDesastre().getLatitud(), asignacion.getDesastre().getLongitud());
            String url = getUrl(furgonPosicion, desastrePosicion, "driving");
            new FetchURL(this).execute(url, "driving");
        }

        //CONTROLA EL ZOOM PRINCIPAL
        LatLng initialLatLng = new LatLng(41.398523, 2.203274);
        float zoomLevel = 12.0f;
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(initialLatLng, zoomLevel));

        //ELIMINAMOS EL LAYERMAP DEL FICHERO KLM QUE NO LO USAREMOS(SOLO HEMOS EXTRAIDOS SUS DATOS)
        kmlLayer.removeLayerFromMap();
    }


    //CARGAR CADA X SEGUNDOS

    //To start timer
    private void startTimer(){
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run(){
                        //your code is here
                        try {
                            leerDirectorioFicheros();
                            extraerDatosFicheros();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        timer.schedule(timerTask, 20000, 20000);
    }

    //To stop timer
    private void stopTimer(){
        if(timer != null){
            timer.cancel();
            timer.purge();
        }
    }

    private void leerDirectorioFicheros() throws FileNotFoundException {
        if(UtilitiesAssignmentXLSFile.directoryExcel != null){
            Collection<File> files = FileUtils.listFiles(UtilitiesAssignmentXLSFile.directoryExcel, new String[]{"xlsx"}, true);
            if(!files.isEmpty()){
                File file = files.iterator().next();
                UtilitiesAssignmentXLSFile.inputStream = new FileInputStream(file);
            }
        }else if(UtilitiesAssignmentKlmFile.directoryKML != null){
            Collection<File> files = FileUtils.listFiles(UtilitiesAssignmentKlmFile.directoryKML, new String[]{"kml"}, true);
            if(!files.isEmpty()){
                File file = files.iterator().next();
                UtilitiesAssignmentKlmFile.inputStream = new FileInputStream(file);
            }
        }
    }

    //Volvemos a extraer los datos de los ficheros y los guardamos
    private void extraerDatosFicheros(){
        //Excel
        try {
            //LEE LICHEROS XLSX
            DataListExcel dataListExcel = new DataListExcel();
            dataListExcel.readXlsxFileFromAssets();
        } catch (IOException e) {
            e.printStackTrace();
        }
        UtilitiesAssignmentXLSFile.filtrarListaExcel(UtilitiesAssignmentXLSFile.tipologiaFiltroList);

        //KML
        onResume();
    }
}