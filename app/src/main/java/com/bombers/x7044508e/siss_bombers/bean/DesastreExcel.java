package com.bombers.x7044508e.siss_bombers.bean;

public class DesastreExcel{

    private int numero;
    private String utm_x;
    private String utm_y;
    private String grup;
    private String per;
    private String veh;
    private String tipus;
    private String municipi;
    private String urgent;
    private String nDocs;
    private String trge;
    private String issi_trge;
    private String numeroCatastrofes;
    private String fechaActualizacion;
    private String nivelPrioridad;

    public DesastreExcel() {
    }

    public DesastreExcel(int numero, String utm_x, String utm_y, String grup, String per, String veh, String tipus, String municipi, String urgent, String nDocs, String trge, String issi_trge, String numeroCatastrofes, String fechaActualizacion, String nivelPrioridad) {
        this.numero = numero;
        this.utm_x = utm_x;
        this.utm_y = utm_y;
        this.grup = grup;
        this.per = per;
        this.veh = veh;
        this.tipus = tipus;
        this.municipi = municipi;
        this.urgent = urgent;
        this.nDocs = nDocs;
        this.trge = trge;
        this.issi_trge = issi_trge;
        this.numeroCatastrofes = numeroCatastrofes;
        this.fechaActualizacion = fechaActualizacion;
        this.nivelPrioridad = nivelPrioridad;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getUtm_x() {
        return utm_x;
    }

    public void setUtm_x(String utm_x) {
        this.utm_x = utm_x;
    }

    public String getUtm_y() {
        return utm_y;
    }

    public void setUtm_y(String utm_y) {
        this.utm_y = utm_y;
    }

    public String getGrup() {
        return grup;
    }

    public void setGrup(String grup) {
        this.grup = grup;
    }

    public String getPer() {
        return per;
    }

    public void setPer(String per) {
        this.per = per;
    }

    public String getVeh() {
        return veh;
    }

    public void setVeh(String veh) {
        this.veh = veh;
    }

    public String getTipus() {
        return tipus;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public String getMunicipi() {
        return municipi;
    }

    public void setMunicipi(String municipi) {
        this.municipi = municipi;
    }

    public String getUrgent() {
        return urgent;
    }

    public void setUrgent(String urgent) {
        this.urgent = urgent;
    }

    public String getnDocs() {
        return nDocs;
    }

    public void setnDocs(String nDocs) {
        this.nDocs = nDocs;
    }

    public String getTrge() {
        return trge;
    }

    public void setTrge(String trge) {
        this.trge = trge;
    }

    public String getIssi_trge() {
        return issi_trge;
    }

    public void setIssi_trge(String issi_trge) {
        this.issi_trge = issi_trge;
    }

    public String getNumeroCatastrofes() {
        return numeroCatastrofes;
    }

    public void setNumeroCatastrofes(String numeroCatastrofes) {
        this.numeroCatastrofes = numeroCatastrofes;
    }

    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public String getNivelPrioridad() {
        return nivelPrioridad;
    }

    public void setNivelPrioridad(String nivelPrioridad) {
        this.nivelPrioridad = nivelPrioridad;
    }

    @Override
    public String toString() {
        return "DesastreExcel{" +
                "numero=" + numero +
                ", utm_x='" + utm_x + '\'' +
                ", utm_y='" + utm_y + '\'' +
                ", grup='" + grup + '\'' +
                ", per='" + per + '\'' +
                ", veh='" + veh + '\'' +
                ", tipus='" + tipus + '\'' +
                ", municipi='" + municipi + '\'' +
                ", urgent='" + urgent + '\'' +
                ", nDocs='" + nDocs + '\'' +
                ", trge='" + trge + '\'' +
                ", issi_trge='" + issi_trge + '\'' +
                ", numeroCatastrofes='" + numeroCatastrofes + '\'' +
                ", fechaActualizacion='" + fechaActualizacion + '\'' +
                ", nivelPrioridad='" + nivelPrioridad + '\'' +
                '}';
    }
}
