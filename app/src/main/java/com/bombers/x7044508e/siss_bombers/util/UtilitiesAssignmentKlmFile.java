package com.bombers.x7044508e.siss_bombers.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bombers.x7044508e.siss_bombers.R;
import com.bombers.x7044508e.siss_bombers.bean.Asignacion;
import com.bombers.x7044508e.siss_bombers.bean.Desastre;
import com.bombers.x7044508e.siss_bombers.bean.DesastreExcel;
import com.bombers.x7044508e.siss_bombers.bean.FurgonSalvamento;
import com.bombers.x7044508e.siss_bombers.geocoordinateconverter.GeoCoordinateConverter;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.data.Geometry;
import com.google.maps.android.data.kml.KmlContainer;
import com.google.maps.android.data.kml.KmlLayer;
import com.google.maps.android.data.kml.KmlPlacemark;
import com.google.maps.android.data.kml.KmlPoint;
import com.koushikdutta.ion.Ion;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ExecutionException;

public class UtilitiesAssignmentKlmFile extends BaseAdapter {
    public static File directoryKML;
    public static InputStream inputStream;
    private Context context;
    public static ArrayList<MarkerOptions> markerOptionsList = new ArrayList<>();

    //VARIABLES GLOBALES DE LA PARTE DE ASGINACION
    private static ArrayList<FurgonSalvamento> furgonSalvamentoList;
    private static ArrayList<Desastre> desastreList;
    private static ArrayList<Asignacion> asignacionList = new ArrayList<>();

    public UtilitiesAssignmentKlmFile(){}

    public UtilitiesAssignmentKlmFile(Context context){
        this.context = context;
    }

    public void convertURIToInputStream(Uri uri) throws FileNotFoundException {
        inputStream = context.getContentResolver().openInputStream(uri);
    }


    //>>>>AQUI EMPIEZA EL CODIGO PARA OBTENER DATOS DEL FICHERO KML<<<<

    public void adaptKmlFileToGoogleMaps(KmlLayer kmlLayer) throws ExecutionException, InterruptedException {
        furgonSalvamentoList = new ArrayList<>();

        if (kmlLayer != null && kmlLayer.getContainers() != null) {
            for (KmlContainer container : kmlLayer.getContainers()) {
                for(KmlContainer container1 : container.getContainers()) {
                    if (container1.hasPlacemarks()) {
                        for (KmlPlacemark placemark : container1.getPlacemarks()) {
                            FurgonSalvamento furgonSalvamento = new FurgonSalvamento();
                            MarkerOptions markerOptions = new MarkerOptions();

                            //Obtener las coordenadas
                            furgonSalvamento.setNombre(placemark.getProperty("name"));
                            furgonSalvamento.setDescripcion(placemark.getProperty("description"));
                            Geometry geometry = placemark.getGeometry();
                            if (geometry.getGeometryType().equals("Point")) {
                                KmlPoint point = (KmlPoint) placemark.getGeometry();
                                furgonSalvamento.setLatitud(point.getGeometryObject().latitude);
                                furgonSalvamento.setLongitud(point.getGeometryObject().longitude);
                            }

                            //Se rellena los markerOptions
                            Bitmap bitmapIcon = getBitmapFromUrlIcon(placemark, container);
                            LatLng latLng = new LatLng(furgonSalvamento.getLatitud(), furgonSalvamento.getLongitud());
                            String descripcion = furgonSalvamento.getDescripcion();
                            String nombre = furgonSalvamento.getNombre();
                            markerOptions.position(latLng).title("Nom: " + nombre + "\n" + descripcion).icon(BitmapDescriptorFactory.fromBitmap(bitmapIcon));

                            //Añadimos el MarkerOptions y el bombero a sus respectivas listas
                            furgonSalvamentoList.add(furgonSalvamento);
                            markerOptionsList.add(markerOptions);
                        }
                    }
                }
            }
        }
    }

    //Se pone el @Target con la version de la api con la que sedebe ejecutar este método(da problemas en ".createBitmap())
    @TargetApi(Build.VERSION_CODES.O)
    private Bitmap getBitmapFromUrlIcon(KmlPlacemark kmlPlacemark, KmlContainer kmlContainerStyle) throws ExecutionException, InterruptedException {
        if(kmlPlacemark != null && kmlContainerStyle != null){
            Paint paint = new Paint();
            ColorFilter filter;

            if((kmlContainerStyle.getStyle(kmlPlacemark.getStyleId() + "_n") != null && kmlContainerStyle.getStyle(kmlPlacemark.getStyleId() + "_n").getIconUrl() != null)){
                String urlIcon = kmlContainerStyle.getStyle(kmlPlacemark.getStyleId() + "_n").getIconUrl();
                Bitmap bitmapIcon = Ion.with(context).load(urlIcon).asBitmap().get().copy(Bitmap.Config.ARGB_8888, true);
//                bitmapIcon = Bitmap.createBitmap(bitmapIcon.getWidth()+20, bitmapIcon.getHeight() + 20, Bitmap.Config.ARGB_8888, true);
                Bitmap bitmapIconResize = Bitmap.createScaledBitmap(bitmapIcon, bitmapIcon.getWidth()+40, bitmapIcon.getHeight() + 40,false);

                if(kmlPlacemark.getStyleId().startsWith("#w_verd")){
                    filter = new PorterDuffColorFilter(ContextCompat.getColor(context, R.color.colorGreenLime), PorterDuff.Mode.SRC_IN);
                }else if(kmlPlacemark.getStyleId().startsWith("#w_groc") || kmlPlacemark.getStyleId().startsWith("#w_descarrega")){
                    filter = new PorterDuffColorFilter(ContextCompat.getColor(context, R.color.colorYellow), PorterDuff.Mode.SRC_IN);
                }else{
                    filter = new PorterDuffColorFilter(ContextCompat.getColor(context, R.color.colorRed), PorterDuff.Mode.SRC_IN);
                }

                paint.setColorFilter(filter);
                Canvas canvas = new Canvas(bitmapIconResize);
                canvas.drawBitmap(bitmapIconResize,0, 0, paint);

                return bitmapIconResize;
            }else if(kmlPlacemark.getStyleId().equals("#w_descarrega")){
                String urlIcon = kmlContainerStyle.getStyle(kmlPlacemark.getStyleId()).getIconUrl();
//                Bitmap bitmapIcon = Ion.with(context).load(urlIcon).asBitmap().get();
                Bitmap bitmapIcon = Ion.with(context).load(urlIcon).asBitmap().get().copy(Bitmap.Config.ARGB_8888, true);
//                bitmapIcon = Bitmap.createBitmap(bitmapIcon.getWidth()+20, bitmapIcon.getHeight() + 20, Bitmap.Config.ARGB_8888, true);
                Bitmap bitmapIconResize = Bitmap.createScaledBitmap(bitmapIcon, bitmapIcon.getWidth()+40, bitmapIcon.getHeight() + 40, false);
                filter = new PorterDuffColorFilter(ContextCompat.getColor(context, R.color.colorYellow), PorterDuff.Mode.SRC_IN);


                paint.setColorFilter(filter);
                Canvas canvas = new Canvas(bitmapIconResize);
                canvas.drawBitmap(bitmapIconResize, 0, 0, paint);

                return bitmapIconResize;
            }
        }
        return null;
    }




    //>>>>AQUI EMPIEZA EL CODIGO PARA ASIGNAR BOMBEROS A INUNDACIONES<<<<
    public static ArrayList<Asignacion> getAssignmentsList(){
        rellenarDesastreLista();
        ordenar(desastreList);
        compararYAsignar(desastreList, furgonSalvamentoList);

        return asignacionList;
    }

    private static void rellenarDesastreLista(){
        desastreList = new ArrayList<>();

        for(DesastreExcel desastreExcel: UtilitiesAssignmentXLSFile.desastreExcelFiltradoList){
            StringBuilder ubicacionUTM = new StringBuilder("31T").append(" ").append(desastreExcel.getUtm_x()).append(" ").append(desastreExcel.getUtm_y());
            double[] coordenada = GeoCoordinateConverter.getInstance().utm2LatLon(ubicacionUTM.toString());

            desastreList.add(new Desastre(desastreExcel.getNumero(), coordenada[0], coordenada[1],Integer.parseInt(desastreExcel.getNivelPrioridad())));
        }
    }

    private static void compararYAsignar(ArrayList<Desastre> desastreList, ArrayList<FurgonSalvamento> furgonSalvamentoList) {
        for(Desastre desastre : desastreList){
            asignarFurgonSalvamento(desastre, furgonSalvamentoList);
        }
    }

    //Medimos la distancia entre el listado de bomberos y el desastre pasado como parametro para obetner la distancia más corta.
    //Luego eliminamos el bombero de la lista para no asignarlo a otro desastre.
    private static void asignarFurgonSalvamento(Desastre desastre, ArrayList<FurgonSalvamento> furgonSalvamentoList) {
        Double distanciaMasCorta = null;
        Asignacion asignacion = new Asignacion();
        FurgonSalvamento furgonSalvamentoEliminar = new FurgonSalvamento();

        for(FurgonSalvamento furgonSalvamento : furgonSalvamentoList){
            Double distancia = SphericalUtil.computeDistanceBetween(new LatLng(desastre.getLatitud(), desastre.getLongitud()), new LatLng(furgonSalvamento.getLatitud(),furgonSalvamento.getLongitud()));
//            distanciaMasCorta = distanciaMasCorta == null || distanciaMasCorta >= distancia ? distancia : distanciaMasCorta;

            if(distanciaMasCorta == null || distanciaMasCorta > distancia){
                distanciaMasCorta = distancia;
                furgonSalvamentoEliminar = furgonSalvamento;
                asignacion = new Asignacion(furgonSalvamento, desastre, desastre.getNivelPrioridad(), distanciaMasCorta, null);
            }
        }
        //Rellenamos el listado de asignaciones.
        asignacionList.add(asignacion);

        //Elimianmos e lbombero asignado de la lista de bomberos disponibles.
        //Faltar probar, si no funciona probar con java.util.Iterator
        furgonSalvamentoList.remove(furgonSalvamentoEliminar);

        //Falta controlar el mostrar los bomberosque no han sido asignadosa ninguna inundación.
    }

    //Se ordena el listado de desastres, por orden de nivel de prioridad para recorrerlos en ese orden y asignarle un bombero
    private static void ordenar(ArrayList<Desastre> desastreList){
//        desastreList.stream().sorted(Comparator.comparing(Desastre::getNivelPrioridad)).collect(Collector.toList());
        Collections.sort(desastreList, new Comparator<Desastre>(){
            public int compare(Desastre o1, Desastre o2){
                if(o1.getNivelPrioridad() == o2.getNivelPrioridad())
                    return 0;
                return o1.getNivelPrioridad() < o2.getNivelPrioridad() ? -1 : 1;
            }
        });
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }
}
